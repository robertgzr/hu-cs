FROM alpine:3.9

RUN apk add -U --no-cache \
        gcc \
        git \
        make \
        meson \
        musl-dev

COPY ./mesonbuild.sh /usr/local/bin/mesonbuild

WORKDIR /src

CMD ["mesonbuild"]
