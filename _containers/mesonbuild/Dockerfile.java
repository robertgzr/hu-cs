FROM docker.io/anapsix/alpine-java:8_jdk

RUN apk add --no-cache \
        git \
        make \
        meson

COPY ./mesonbuild.sh /usr/loca/bin/mesonbuild

WORKDIR /src

CMD ["mesonbuild"]
