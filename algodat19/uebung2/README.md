In dieser Aufgabe geht es um das Auswerten von arithmetischen Ausdrücken, welche ausschließlich aus Ziffern, öffnenden und schließenden Klammern und den Operatoren +,−,∗,/ bestehen. Hierbei steht der Operator „/“ für Ganzzahldivision.
Wir definieren einen _wohlgeformten arithmetischen Ausdruck_ wie folgt:

- Eine Ziffer `d ∈ {0,1,2,3,4,5,6,7,8,9}` ist ein wohlgeformter arithmetischer Ausdruck.
- Sind `X` und `Y` wohlgeformte arithmetische Ausdrücke, dann sind auch die vier Ausdrücke `(X+Y)`, `(X−Y)`, `(X∗Y)` und `(X/Y)` wohlgeformte arithmetische Ausdrücke.

Beispiele für wohlgeformte arithmetische Ausdrücke entsprechend dieser Definition sind unteranderem: 
`((8 + 7)∗2)`, `(4−(7−1))` oder `8`. 
Bei den Ausdrücken `)8+)1(())`, `(8 + ())`,`−1`, `(5−7)`, `108` oder `(8)` handelt es sich hingegen nicht um wohlgeformte arithmetische Ausdrücke.

Schreiben Sie ein Java-Programm, welches für eine übergebene Zeichenkette überprüft, ob es sichum einen wohlgeformten arithmetischen Ausdruck handelt.
Falls es sich um einen wohlgeformtenarithmetischen Ausdruck handelt, so soll Ihr Algorithmus das Ergebnis dieses Ausdrucks ausrechnen und als Integer-Wert zurückgeben. 
Andernfalls soll eine `ExpressionNotWellFormedException` geworfen werden. 
Für diese Aufgabe bietet sich die Verwendung eines Stacks (`java.util.Stack`) an.Zur Vereinfachung der Implementierung, brauchen Sie beim Ausrechnen des Ergebnisses Divisionen durch 0 sowie arithmetische Überläufe nicht gesondert behandeln.

---

```
$ make build test # with java installed
# or
$ make cbuild ctest # to use a container runtime
```
