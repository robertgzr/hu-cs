package berlin.hu;

import java.util.Stack;
import java.util.Arrays;

/**
 * A class for parsing arithmetic expressions
 */
public class Parser {

  public static int passedTests = 0;

  /**
   * An exception that is thrown if the to-be-parsed expression is not
   * well-formed.
   */
  public static class ExpressionNotWellFormedException extends Exception {
    public ExpressionNotWellFormedException() {
    }
  }

  static int charAtoi(char a) {
    return Integer.parseInt(Character.toString(a));
  }

  static boolean isOp(char ch) {
    Character[] ops = { '+', '-', '*', '/' };
    return Arrays.asList(ops).contains(ch);
  }

  /**
   * Parses a given String, determines whether it is a well-formed expression,
   * and computes the expression.
   * 
   * @param expression
   *          the expression that is to be evaluated
   * @return the result of the evaluation / computation
   * @throws ExpressionNotWellFormedException
   *           if the expression is not well-formed, an exception is thrown
   */
  public static int parse(String expression)
      throws ExpressionNotWellFormedException {


    String rpn = toRPN(expression);
    int r = evalRPN(rpn);

    return r;
  }

  static String toRPN(String expression)
    throws ExpressionNotWellFormedException {

    Stack<Character> stack = new Stack<Character>();
    String out = "";
    boolean hasSeenOp = false;
    boolean hasSeenBrackets = false;

    for (int i=0; i < expression.length(); ++i) {
      char ch = expression.charAt(i);

      if (Character.isSpaceChar(ch)) {
        // saw whitespace, fail early
        throw new ExpressionNotWellFormedException();
      }
      if (isOp(ch)) {
        // look for ops on the stack, and append
        while (!stack.empty() && stack.peek() != '(') {
          out += stack.pop();
        }
        // push this one
        stack.push(ch);
        // register that we've seen an op
        hasSeenOp = true;
        continue;
      }

      if (ch == '(') {
        // indicate to the next conditional of a later iteration
        // that we saw an opening bracket
        stack.push(ch);
        continue;
      }

      if (ch == ')') {
        // pop and append ops from the stack, until '('
        while (!stack.empty()) {
          char top = stack.pop();
          if (top == '(') {
            hasSeenBrackets = true;
            break;
          }
          out += top;
        }
        continue;
      }

      // found operand, append immediately
      out += ch;
    }

    // pop and append remaining ops, if any
    while (!stack.empty()) {
      if (isOp(stack.peek())) {
        out += stack.pop();
      } else {
        throw new ExpressionNotWellFormedException();
      }
    }

    // apply rules
    // 1. expr has more than one operand and no ops
    // 2. we have seen brackets but no op
    if ((out.length() > 1 && !hasSeenOp) || (hasSeenBrackets && !hasSeenOp) || (!hasSeenBrackets && hasSeenOp)) {
      throw new ExpressionNotWellFormedException();
    }

    return out;
  }

  // rpn makes evaluation trivial :)
  static int evalRPN(String rpn)
    throws ExpressionNotWellFormedException {

    Stack<Integer> stack = new Stack<Integer>();
    int r = 0;
    for (int i=0; i < rpn.length(); ++i) {
      try {
        char ch = rpn.charAt(i);
        if (isOp(ch)) {
          int rhs = stack.pop();
          int lhs = stack.pop();
          int ir = 0;
          switch (ch) {
            case '+':
              ir = lhs + rhs;
              break;
            case '-':
              ir = lhs - rhs;
              break;
            case '*':
              ir = lhs * rhs;
              break;
            case '/':
              ir = lhs / rhs;
          }
          stack.push(ir);
        } else {
          stack.push(charAtoi(ch));
        }
      } catch (java.util.EmptyStackException e) {
        // catch expressions with unbalanced brackets or operands
        throw new ExpressionNotWellFormedException();
      }
    }

    r = stack.pop();
    return r;
  }

  /**
   * test cases
   */
  public static void main(String[] args) {
    {
      wellFormedCheck("((8+7)*2)", 30);
      wellFormedCheck("(4-(7-1))", -2);
      wellFormedCheck("8", 8);
      wellFormedCheck("((1+1)*(2*2))", 8);

      notWellFormedCheck(")8+)1(())");
      notWellFormedCheck("(8+())");
      notWellFormedCheck("-1");
      notWellFormedCheck("(   5    -7)");
      notWellFormedCheck("108");
      notWellFormedCheck("(8)");
      notWellFormedCheck("1*1");
      notWellFormedCheck("(3-2)(");
    }

    if (passedTests != 10) {
      System.exit(1);
    } else {
      System.exit(0);
    }
  }

  private static void checkAndPrint(String message, boolean correct) {
    passedTests += correct ? 1 : 0;
    System.out.println((correct ? "PASS:" : "FAIL:") + " " + message);
    assert (correct);
  }

  private static void notWellFormedCheck(String expression) {
    try {
      int returned = parse(expression);
      checkAndPrint("nicht wohlgeformter Ausdruck " + expression
          + " ausgewertet zu " + returned, false);
    } catch (ExpressionNotWellFormedException e) {
      checkAndPrint("Ausdruck " + expression
          + " als nicht wohlgeformt erkannt.", true);
    }
  }

  private static void wellFormedCheck(String expression, int expected) {
    try {
      int returned = parse(expression);
      checkAndPrint("Ausdruck " + expression + " ausgewertet zu " + returned
          + " (erwartet: " + expected + ")", returned == expected);
    } catch (ExpressionNotWellFormedException e) {
      checkAndPrint("Ausdruck " + expression
          + " fälschlicherweise als nicht wohlgeformt eingeschätzt.", false);
    }
  }
}
