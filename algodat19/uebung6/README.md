In dieser Aufgabe sollen Sie mit Hilfe der [Huffman-Kodierung][1] einen Text kodieren und dekodieren.
Die Huffmann-Kodierung ordnet einer festen Menge von Quellsymbolen jeweils Codewörter variabler Länge zu.
Dazu werden zunächst die Häufigkeiten der einzelnen Symbole bestimmt und in einer Priority Queue verwaltet.
Danach wird, wie in der Vorlesung kennengelernt (Folie10, Priority Queues), in einem Bottom-Up Verfahren der binäre Huffman-Baum erstellt, um den Huffman Code für alle Symbole zu bestimmen.

Betrachten Sie für die Implementierung die auf der Übungswebsite und Moodle bereitgestellten Vorlagen `Huffman.java`. Die Klasse enthält die folgenden zwei privaten Variablen:

* `Node root`: Speichert die Wurzel des binären Huffman-Baums für die Kodierung.

* `Map<Character, String> code`: Speichert die Zuordnung eines Buchstaben zu seiner Kodierung.

Zusätzlich ist die Methode `encode` schon implementiert, in welcher die Häufigkeiten aller Buchstaben bestimmt werden und für jeden Buchstaben ein Knoten erstellt wird, welche nach Häufigkeit geordnet in einer Priority Queue verwaltet werden. 
Anschließend soll mit Hilfe der Methode `createTree` der binäre Huffman-Baum erstellt und danach mit Hilfe der Methode `createCode` für jeden Buchstaben die dazugehörige Kodierung bestimmt werden, um letztlich den übergebenenString zu kodieren.

Ergänzen Sie die Vorlage `Huffman.java` indem Sie folgende Funktionen implementieren:

* `void createTree(PriorityQueue<Entry> pq)`: Erstellt den binären Baum für die Kodierung anhand der übergebenen Priority Queue und speichert die Wurzel in der privaten Variable `root`.

* `void createCode(Nodenode, String prefix)`: Speichert die Zuordung eines jeden Buchstabenzu seiner Kodierung in der privaten Variable `code`. Dazu wird der binäre Baum mit deraktuellen (Teil-)Kodierung traversiert. Der erstmalige Aufruf erfolgt mit der Wurzel des binären Huffman-Baums und einem leeren String.

* `String decode(String input)`: Dekodiert den String `input` (bestehend aus 0 und 1) anhand des binären Huffman-Baums.

Zum Testen können Sie die main-Methode in der Vorlage `Huffman.java` verwenden. Benutzen Sie dafür die Testdateien `lorem1.txt`,`lorem2.txt` und `lorem3.txt`. Sie können beliebige neue Variablen und Hilfsmethoden zur Klasse hinzufügen, dürfen jedoch keine außer den von Java bereitgestellten Standard-Bibliotheken verwenden.

[1]: https://www.youtube.com/watch?v=JsTptu56GM8

---

```
$ make build test # with java installed
# or
$ make cbuild ctest # to use a container runtime
```
