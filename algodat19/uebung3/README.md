In dieser Aufgabe sollen Sie eine einfach verkettete Liste und die folgenden dazugehörigen Methoden implementieren. 
Eine einfach verkettete Liste ist eine Folge von Knoten und jeder Knotenbesteht aus einem Wert ( *Integer* ) und einem Zeiger zu seinem nachfolgenden Knoten. 
Der Zeigeristnull, wenn der Knoten der letzte in der Liste ist. Der erste Knoten in der Liste wird als Kopf ( *head* ) bezeichnet und hat die Position *0* und *n* ist die Anzahl der Elemente der Liste.

### Erster Teil

Implementieren Sie die folgenden Standardmethoden einer verketteten Liste, welche in der Datei `LinkedListExercise.java` schon deklariert, aber noch nicht implementiert sind:

1. `nodeAt(int pos)`: 
    Gibt den Knoten an Positionpos, mit `0 ≤ pos < n`, der Liste zurück.
    Falls die übergebene Position nicht existiert, wird eine `InvalidPositionException` geworfen.
    Nutzen Sie dafür `throw new InvalidPositionException();` 

2. `insert(int val, int pos)`: 
    Fügt einen neuen Knoten mit Wertvalan die Positionposder Liste ein. 
    Falls `pos < 0` oder `pos > n` ist, wird eine `InvalidPositionException` geworfen.

3. `elementAt(int pos)`:
    Gibt den Wert des Knoten an Positionposder Liste zurück. 
    Falls die übergebene Position nicht existiert, wird eine `InvalidPositionException` geworfen.

4. `delete(int pos)`: 
    Löscht den Knoten an Positionposin der Liste. Falls die übergebene Position nicht existiert, wird eine `InvalidPositionException` geworfen.

### Zweiter Teil

Nachdem Sie die vier Standardmethoden implementiert haben, benutzen wir diese, um komplexere Methoden zu implementieren. 
Implementieren Sie dazu die folgenden Methoden in der Datei `LinkedListExercise.java`:

1. `reverse()`: 
    Diese Methode dreht den Inhalt der Liste um;
    Zum Beispiel soll eine Liste 1→2→3 nach Aufruf von `reverse()` die Form 3→2→1 haben.

2. `split(int val)`: 
    Ordnet die Knoten der Liste so um, dass jeder Knoten mit einem Wert≥valvor jedem Knoten mit einem Wert `< val` kommt. 
    Eine Liste 1→4→3→2→5 soll also nach einem Aufruf von `split(int val)` mit `val=3` zum Beispiel die Form 3→4→5→1→2 haben. 
    Nach der split-Operation darf die Reihenfolge der Knoten mit Wert `≥ val` und die Reihenfolge der Knoten mit Wert `< val` beliebig sein.

**Hinweis:** Verwenden Sie zur Realsierung von `reverse()` und `split()` ausschließlich den in `LinkedListExercise.java` definierten ADT LinkedList und die im ersten Teil realisierten Methoden. 
Sie können dabei beliebige Hilfsmethoden zur Klasse hinzufügen, dürfen jedoch wederweitere Klassen der Java-Standardbibliothek noch externen Code verwenden. 
Für `reverse()` und `split()` darf Ihr Algorithmus *O(n)* zusätzlichen Speicherplatz benutzen (womöglich umeine temporäre Liste zu bauen). 
Die volle Punktzahl gibt es jeweils für eine Laufzeit von *O(n)*. Die Vorlage `LinkedListExercise.java` finden Sie auf der Übungswebseite und auf Moodle.

Stellen Sie sicher, dass alle Testfälle in der main-Methode der Datei `LinkedListExercise.java` erfolgreich durchlaufen. 
Stellen Sie sicher, dass Java-Assertions ausgewertet werden.

---

```
$ make build test # with java installed
# or
$ make cbuild ctest # to use a container runtime
```
