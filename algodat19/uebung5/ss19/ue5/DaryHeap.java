package ss19.ue5;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

/**
 * Diese Klasse implementiert die Datenstruktur d-närer Min-Heap als heapgeordnetes Array.
 *
 * @param <T>
 *            Klasse der Objekte, die im d-nären Min-Heap abgelegt werden sollen
 */
public class DaryHeap<T extends Comparable<T>> {

	// das heapgeordnete Array, in dem die Elemente des Heaps gespeichert werden
	private List<T> elements;
	// die Obergrenze für die Anzahl der Kinder eines Elements des Heaps
	private final int d;

	public DaryHeap(int d) {
		this.elements = new ArrayList<>();
		this.d = d;
	}

	/**
	 * Hilfsmethode zum Tauschen zweier Elemente mit Indizes i und j im heapgeordneten Array.
	 * 
	 * @param i
	 *            Index des ersten zu tauschenden Elements im heapgeordneten Array
	 * @param j
	 *            Index des zweiten zu tauschenden Elements im heapgeordneten Array
	 */
	protected void swap(int i, int j) {
		T elementI = elements.get(i);
		T elementJ = elements.get(j);
		elements.set(j, elementI);
		elements.set(i, elementJ);
	}

	/**
	 * Hilfsmethode zum "Heruntersickern" des Elements mit Index i im heapgeordneten Array.
	 * 
	 * @param i
	 *            Index des Elements, das im heapgeordneten Array heruntersickern soll
	 */
	protected void siftDown(int i) {
		// zunächst wird das aktuelle Element (Index i) mit all seinen Kindern verglichen und das kleinste Element wird ermittelt
		int smallest = i;
		for (int pos = 1; pos <= d; pos++) {
			int child = d * i + pos;
			if (child < size() && elements.get(child).compareTo(elements.get(smallest)) < 0) {
				smallest = child;
			}
		}

		// wenn das kleinste ermittelte Element ein Kind ist, dann wird mit ihm getauscht und von dort weiter heruntergesickert
		if (smallest != i) {
			swap(i, smallest);
			siftDown(smallest);
		}
	}

	/**
	 * Methode zum Ausgeben und Löschen des kleinsten Elements (also des Wurzelelements) im heapgeordneten Array.
	 * 
	 * @return das kleinste Element (also das Wurzelelement) des heapgeordneten Array
	 */
	public T deleteMin() {
		// Das Wurzelelement wird ermittelt, mit dem letzten Element vertauscht und gelösscht
		if (this.size() == 0) {
			return null;
		}
		T min = this.elements.get(0);
		int n = this.size();

		swap(0, n - 1);
		this.elements.remove(n - 1);

		// anschließend sickert das neue Wurzelelement herunter und das alte Wurzelelement wird zurückgegeben
		siftDown(0);
		return min;
	}

	/**
	 * Methode zum Erstellen eines neuen heapgeordneten Arrays aus einer übergebenen Liste.
	 * 
	 * @param list
	 *            Liste von Elementen, aus der ein neues heapgeordnetes Array erstellt werden soll
	 */
	public void build(List<T> list) {
		// set internal list to parameter list
		this.elements.addAll(list);
		// iterate backwards beginning with at depth max-1 (=excluding leaf nodes)
		for (int pos = (this.size()-1)/this.d; pos>=0 ; pos--) {
			// bubble down to fullfill the heap constraint
			this.siftDown(pos);
		}
	}

	/**
	 * Hilfsmethode zum "Hochsickern" des Elements mit Index i im heapgeordneten Array.
	 * 
	 * @param i
	 *            Index des Elements, das im heapgeordneten Array hochsickern soll
	 */
	protected void siftUp(int i) {
		// check if we're not at root already
		if (i > 0) {
			int parentIdx = (i-1)/this.d;
			// check if the element at i is larger than it's parent node
			if (this.elements.get(i).compareTo(this.elements.get(parentIdx)) < 0) {
				// if so swap
				this.swap(i, parentIdx);
				// and check again with the new parent and it's parents
				this.siftUp(parentIdx);
			}
		}
	}

	/**
	 * Methode zum Hinzufügen des neuen Element element zum heapgeordneten Array.
	 * 
	 * @param element
	 *            das neu hinzuzufügende Element
	 */
	public void add(T element) {
		int idx = this.size();
		// insert at the end (as a leaf node)
		this.elements.add(idx, element);
		// bubble it up (if required)
		this.siftUp(idx);
	}

	/**
	 * Methode zum Ermitteln aller Elemente im heapgeordneten Array, die kleiner als ein übergebenes Element sind.
	 * 
	 * @param element
	 *            das Element, das mit Elementen im heapgeordneten Array verglichen werden soll
	 * @return Elemente im heapgeordneten Array, die kleiner als das übergebene Element sind
	 */
	public List<T> smallerAs(T element) {
		// create working copy
		DaryHeap<T> h = new DaryHeap(this.d);
		h.build(this.elements);

		List<T> elementsSmaller = new ArrayList<>();
		// extract min from heap as long as it's smaller than input
		T min = h.deleteMin();
		while (!h.isEmpty() && min.compareTo(element) < 0) {
			elementsSmaller.add(min);
			min = h.deleteMin();
		}
		return elementsSmaller;
	}

	/**
	 * Sortiert eine Liste aufsteigend unter Verwendung eines heapgeordneten Arrays.
	 * 
	 * @param list
	 *            die aufsteigend zu sortierende Liste
	 */
	public static <T extends Comparable<T>> void heapSort(List<T> list) {
		// initialize binary min heap with input list
		DaryHeap<T> h = new DaryHeap(2);
		h.build(list);
		// clear input
		list.clear();
		// extract min from the heap until empty
		while(!h.isEmpty()) {
			list.add(h.deleteMin());
		}
	}

	/**
	 * Hilfsmethode, die überprüft, ob die Heap-Eigenschaft erfüllt ist.
	 */
	protected void check() {
		for (int i = 0; i < size(); i++) {
			for (int pos = 1; pos <= d; pos++) {
				int offset = d * i + pos;
				if (offset < size() && elements.get(i).compareTo(elements.get(offset)) > 0) {
					System.err.println("Min-Heap-Error. Vater-Knoten " + elements.get(i) + " ist kleiner als sein Kind " + elements.get(offset));
				}
			}
		}
	}

	@Override
	public String toString() {
		return this.elements.toString();
	}

	/**
	 * Methode, die die Größe des heapgeordneten Arrays ausgibt.
	 * 
	 * @return die Größe des heapgeordneten Arrays
	 */
	public int size() {
		return elements.size();
	}

	/**
	 * Methode, die ausgibt, ob das heapgeordnete Array leer ist.
	 * 
	 * @return true, gdw das heapgeordnete Array leer ist.
	 */
	public boolean isEmpty() {
		return elements.isEmpty();
	}

	public boolean equals(List<T> o) {
		return this.elements.equals(o);
	}

	public static void tests() throws Exception {
		// Generiere eine Liste der Zahlen von 30 bis 21 und 1 bis 10
		List<Integer> list = new ArrayList<>();
		for (int i = 30; i > 20; i--) {
			list.add(i);
		}
		for (int i = 1; i <= 10; i++) {
			list.add(i);
		}

		DaryHeap<Integer> minHeap = new DaryHeap<>(4);

		// empty heap checks
		System.out.println("Check deleteMin on empty heap.");
		minHeap.deleteMin();

		System.out.println("Check smallerAs on empty heap.");
		List<Integer> smaller99 = minHeap.smallerAs(99);
		if (!smaller99.isEmpty()) {
			throw new Exception();
		}

		System.out.println("Check heapSort on empty list.");
		heapSort(smaller99);
		System.out.println();
		if (!smaller99.isEmpty()) {
			throw new Exception();
		}

		// Erstelle daraus einen 4-nären Min-Heap
		System.out.println("Erstelle Liste:\t\t" + list.toString());
		minHeap.build(list);

		// Prüfe, ob der Min-Heap korrekt erstellt wurde.
		minHeap.check();
		List<Integer> target1 = Arrays.asList(1, 22, 2, 4, 8, 25, 24, 23, 29, 21, 28, 30, 3, 27, 5, 6, 7, 26, 9, 10);
		System.out.println("Baue Heap aus Liste:\t" + minHeap.toString());
		System.out.println("Er sollte so aussehen:\t" + target1.toString());
		System.out.println();
		if (!minHeap.equals(target1)) {
			throw new Exception();
		}

		// Ermittle alle Elemente kleiner als 5 und kleiner als 10
		List<Integer> smaller = minHeap.smallerAs(5);
		System.out.println("Zahlen kleiner als 5:\t" + smaller);
		if (smaller.size() < 1) {
			throw new Exception();
		}
		for (int i : smaller) {
			if (i > 5) {
				throw new Exception();
			}
		}
		smaller = minHeap.smallerAs(10);
		System.out.println("Zahlen kleiner als 10:\t" + smaller);
		if (smaller.size() < 1) {
			throw new Exception();
		}
		for (int i : smaller) {
			if (i > 10) {
				throw new Exception();
			}
		}
		smaller = minHeap.smallerAs(1);
		System.out.println("Zahlen kleiner als 1:\t" + smaller);
		if (!smaller.isEmpty()) {
			throw new Exception();
		}
		System.out.println();

		// Extrahiere zwei mal das Minimum aus dem Min-Heap
		for (int i = 0; i < 2; i++) {
			System.out.println("Extrahiere Minimum:\t" + minHeap.deleteMin());
		}

		// Prüfe, ob der Min-Heap noch korrekt ist.
		minHeap.check();
		List<Integer> target2 = Arrays.asList(3, 22, 9, 4, 8, 25, 24, 23, 29, 21, 28, 30, 10, 27, 5, 6, 7, 26);
		System.out.println("Heap nach Extraktion:\t" + minHeap.toString());
		System.out.println("Er sollte so aussehen:\t" + target2.toString());
		System.out.println();
		if (!minHeap.equals(target2)) {
			throw new Exception();
		}

		// Füge die Zahlen von 11 bis 20 zum Min-Heap hinzu
		for (int i = 11; i <= 20; i++) {
			minHeap.add(i);
		}

		// Prüfe, ob der Min-Heap noch korrekt ist.
		minHeap.check();
		List<Integer> target3 = Arrays.asList(3, 14, 9, 4, 8, 15, 18, 23, 29, 21, 28, 30, 10, 27, 5, 6, 7, 26, 11, 12, 13, 25, 22, 16, 17, 24, 19, 20);
		System.out.println("Füge 11 bis 20 hinzu:\t" + minHeap.toString());
		System.out.println("Er sollte so aussehen:\t" + target3.toString());
		System.out.println();
		if (!minHeap.equals(target3)) {
			throw new Exception();
		}

		System.out.println("Extrahiere alle Werte.");
		int lastMin = Integer.MIN_VALUE;
		while (!minHeap.isEmpty()) {
			minHeap.check();
			int min = minHeap.deleteMin();
			if (min < lastMin) {
				System.out.println("Minimum nicht korrekt: " + min + "<" + lastMin);
			} else {
				System.out.print(".");
			}
			lastMin = min;
		}
		System.out.println("\n");

		// Sortiere die ursprünglich erstellte Liste der Zahlen von 20 bis 1 mittels Heapsort
		list = new ArrayList<>();
		for (int i = 20; i > 0; i--) {
			list.add(i);
		}
		System.out.println("Erstelle Liste:\t\t" + list.toString());
		heapSort(list);
		List<Integer> target4 = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20);
		System.out.println("Sortiere mit Heapsort:\t" + list.toString());
		System.out.println("Sie sollte so aussehen:\t" + target4.toString());
		System.out.println();
		if (!list.equals(target4)) {
			throw new Exception();
		}
	}

	public static void main(String[] argv) {
		try {
			tests();
			System.exit(0);
		} catch (Exception e) {
			System.out.println("[ FAIL ]");
			System.exit(1);
		}
	}
}
