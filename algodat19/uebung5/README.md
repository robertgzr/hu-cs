In der Vorlesung haben Sie bereitsbinäre Min-Heaps unter dem Namen Heaps kennengelernt.
In dieser Aufgabe betrachten wir mit d-nären Min-Heaps eine Verallgemeinerung binärer Min-Heaps, in denen Knoten anstatt maximal zwei Kindern, `d >= 2` Kinder haben können. 
Ferner lernen wir eine Methode kennen, solche d-nären Min-Heaps als Array zu repräsentieren und implementieren.

Da es sich um eine Java-Implementierungsaufgabe handelt, beginnt im Folgenden die Indizierung von Arrays bei 0. Für d-näre heapgeordneten Arrays gilt, dass für ein Element an Stelle i des Arrays das erste Kind im Heap an Stelle `d*i+1`, das zweite Kind an Stelle `d*i+2`, . . . , und das d-te Kind an Stelle `d*i+d` im Array zu finden ist. 

Für den in Abbildung 1 dargestellten 3-nären Min-Heap ergibt sich somit das heapgeordnete Array `A=[7,9,12,8,11,33,25,24,17,13,14,10]`.

Für die Implementierung d-närer heapgeordneter Arrays finden Sie auf der [Übungswebsite][1] und Moodle die Vorlage `DaryHeap.java`. Darin sind, analog zu den Beschreibungen in der Vorlesung (Foliensatz „Priority Queues“, Folien 18 ff.) die nachfolgenden Funktionen vorgegeben:

* `siftDown(i)`: Lässt das Element an Stelle i im heapgeordneten Array nach unten sickern (Folie 23). 
Dabei wird es sukzessive mit dem kleinsten Kindelement vertauscht, solange mindestens ein Kindelement kleiner ist.

* `deleteMin()`: Liefert und entfernt das kleinste Element im heapgeordneten Array (Folie23 ff.). 
Nach Entfernen des Minimums wird die Heap-Eigenschaft mittels `siftDown(i)` wiederhergestellt.


![Abb.1: ein 3-närer Min-Heap][2]


Ergänzen Sie die Vorlage `DaryHeap.java` unter Ausnutzung der existierenden Funktionen-Stubs um die nachfolgenden Funktionen:

* `build(list)`: Erstellt ein neues heapgeordnetes Array, welches die Elemente der übergebenen Liste *list* enthält. 
Wie in der Vorlesung beschrieben (Folien 32 ff.) soll dabei nach der *Bottom-Up-Sift-Down-Methode* vorgegangen werden, um die lineare Laufzeit *O(n)* zu erreichen.

* `siftUp(i)`: Lässt das Element an Stelle i im heapgeordneten Array nach oben sickern.
Analog zur Methode `siftDown(i)` und wie in der Vorlesung beschrieben (Folie 23) wird es dazu sukzessive mit Elternelementen verglichen und vertauscht, solange das Elternelement größer ist.

* `add(element)`: Fügt dem heapgeordneten Array ein neues Element *element* hinzu. 
Wie in der Vorlesung beschrieben (Folien 26 ff.) soll dafür die Methode `siftUp(i)` verwendet werden, um die logarithmische Laufzeit *O(logn)* zu erreichen.

* `smallerAs(element)`: Gibt alle Elemente aus dem heapgeordneten Array, die kleiner als *element* sind, als Liste zurück, ohne dabei den Heap zu verändern. Die Laufzeit Ihres Algorithmus soll in *O(k)* sein, wobei *k* der Anzahl der Elemente im Array, die kleiner als *element* sind, entspricht.

Sie können davon ausgehen, dass jedes Element nur einmal in das heapgeordnete Array eingefügt wird. 
Sie brauchen sich also bei der Implementierung der Methoden `build(list)` und `add(element)` nicht um den Umgang mit Duplikaten zu kümmern. 

Zum Testen können Sie die main-Methode in der Vorlage `DaryHeap.java` verwenden. Achten Sie außerdem auf Randbedingungen und Spezialfälle, die von den Testfällen vielleicht nicht vollständig abgedeckt werden.

[1]: https://hu.berlin/algodat19
[2]: abb1_3-naerer-heap.png

---

```
$ make build test # with java installed
# or
$ make cbuild ctest # to use a container runtime
```
