Implementieren Sie die nachfolgenden zwei Suchverfahren für die Suche eines Werts *k* in einem aufsteigend sortierten Array *A* mit Werten vom Typ `Long`:

* (a) *binäre Suche* und
* (b) *Interpolationssuche*.

Gehen Sie für die Implementierung der Interpolationssuche analog zur Vorlesung von gleich-verteilten Daten aus. 

Ergänzen Sie den fehlenden Code in der Vorlage `SortedSearch.java`, welche Sie auf der [Übungswebseite][1] und Moodle vorfinden.
Sie können beliebige neue Variablen und Hilfsmethoden zur Klasse hinzufügen, dürfen jedoch keine außer den von Java bereitgestellten Standard-Bibliotheken verwenden. 
Verwenden Sie für alle Vergleiche zweier Werte im Array *A* die vorgegebene Klasse `CountingComparator`. 
Mittels des Comperators wird die Anzahl der Vergleiche gezählt. 

Ein Beispiel für die Implementierung einer Suche in dem vorgegebenen Quellcode-Gerüst finden Sie in der Klasse `LinearSearch`. 

Stellen Sie sicher, dass alle Testfälle in der `main`-Methode der Datei `SortedSearch.java` erfolgreich durchlaufen. 
Achten Sie außerdem auf Randbedingungen und Spezialfälle, die von den Testfällen vielleicht nicht vollständig abgedeckt werden.

[1]: https://hu.berlin/algodat19

---

```
$ make build test # with java installed
# or
$ make cbuild ctest # to use a container runtime
```
